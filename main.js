const electron = require('electron');
const {ipcMain} = require('electron');
const Application = require('./application/Application');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');

let window;
let application = new Application(electron);

function createWindow ()
{
    window = new BrowserWindow({width: application.width, height: application.height, title: application.name, resizable: true, frame: false, transparent: true});
    
    application.onWindowCreated(window);

    window.loadURL(url.format(
    {
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    //window.webContents.openDevTools();

    window.once('ready-to-show', () => application.onWindowLoaded(window));

    window.on('closed', () => window = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () =>
{
    if (process.platform !== 'darwin')
        app.quit();
});

app.on('activate', () =>
{
    if (window === null)
        createWindow();
});

ipcMain.on("titlebar-title", (event) =>
{
    event.sender.send("titlebar-title-retrieved", {title: application.name});
});
