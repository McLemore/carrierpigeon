const fs = require("fs");

module.exports = class Theme {
    constructor(window, css, script) {
        this.window = window;
        this.css = css;
        this.script = script;
    }

    apply() {
        this.window.webContents.executeJavaScript(
            `$("#css_theme").href = ${JSON.stringify(this.css)};`
        );

        fs.readFile(this.script, (err, contents) => {
            if (err)
                console.error(err);
            else
                this.window.webContents.executeJavaScript(contents.toString());
        });
    }

    static apply(window, css, js) {
        new Theme(window, css, js).apply();
    }
}