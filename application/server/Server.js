module.exports = class Server
{
    connect(address, port)
    {
        // Request Connection
        Server.connected = true;

        // Successful Connection
        if (Server.connected)
        {
            Server.address = address;
            Server.port = port;

            // Send Connect Message

            // Notify Successful Connection
        }
        else
        {
            // Notify Unsuccessful Connection
        }
    }

    static disconnect()
    {
        // Disconnect
        Server.send(""); // Send Disconnect Message
        Server.connected = false;

        // Notify Disconnect
    }

    static send(message)
    {

    }

    static send(message, channel)
    {

    }

    static defaultChannel()
    {
        Server.send(""); // Send Request to get Default Channel

        return null;
    }
}