const Server = require("./server/Server");
const Theme = require("./theme/Theme");

module.exports = class Application
{
    constructor(electron)
    {
        this.electron = electron;

        this.name = "[Carrier Pigeon]";
        this.width = 1280;
        this.height = 720;

        this.activeChannel = Server.defaultChannel();
    }

    onWindowCreated(window)
    {
        this.electron.Menu.setApplicationMenu(null);

        // Load the main view controller
        Theme.apply(window, "index.css", "index.js");

        // TODO: Load the user's chosen theme
        Theme.apply(window, "application/assets/themes/default.css", "application/assets/themes/default.js");
    }

    onWindowLoaded(window)
    {
        
    }
}