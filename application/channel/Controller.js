module.exports = class Controller
{
    constructor(channel)
    {
        this.channel = channel;
        this.children = null;
    }

    broadcast(message)
    {
        // Server.broadcast(message);
    }

    receive(message)
    {
        if (this.children)
        {
            for (let child of this.children)
                child.receive(message);
        }
        else
            handle(message);
    }

    handle(message)
    {
        // Handle the message
    }
}