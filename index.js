const { remote } = require("electron");
const { ipcRenderer } = require("electron");

let windowMaximized = remote.getCurrentWindow().isMaximized();

requestTitle();
applyTitleBarControls();
alignTitleBarControls();

function requestTitle() {
    ipcRenderer.send("titlebar-title");
    ipcRenderer.on("titlebar-title-retrieved", (event, data) => applyTitle(data.title));
}

function applyTitle(title) {
    let titlebarTitle = $("#titlebar-title")[0];
    console.log(titlebarTitle);
    titlebarTitle.innerHTML = title;
}

function applyTitleBarControls() {
    $("#titlebar-minimize").on("click", () => {
        let window = remote.getCurrentWindow();
        window.minimize();
    })

    $("#titlebar-maximize").on('click', () => {
        let window = remote.getCurrentWindow();
        windowMaximized ? window.unmaximize() : window.maximize();
        windowMaximized = !windowMaximized;
    })

    $("#titlebar-close").on('click', () => {
        let window = remote.getCurrentWindow();
        window.close();
    });
}

function alignTitleBarControls() {
    let titlebarControls = $("#titlebar-controls").val();

    if (titlebarControls && titlebarControls.classList.contains("align-left")) {
        for (let child of titlebarControls.childNodes)
            titlebarControls.insertBefore(child, titlebarControls.firstChild);
    }
}